//Ejercicio 1
// Completa la función que tomando dos números como argumento devuelva el más alto.

/*
function sum(numberOne , numberTwo) {
    if  (numberOne > numberTwo){
        return numberOne;
    } else if (numberTwo > numberOne){
        return numberTwo;
    }
  }
sum();
*/

// Ejercicio 2
//Completa la función que tomando un array de strings como argumento devuelva el más largo, en caso de que dos strings tenga la misma longitud deberá devolver el primero.
/*
const avengers = ['Hulk', 'Thor', 'IronMan', 'Captain A.', 'Spiderman', 'Captain M.'];
function findLongestWord(param) {

    let longestWord = param[0];
    for (let i = 1; i < param.length; i++) {
    if (param[i].length > longestWord.length) {
        longestWord = param[i];
    }
    }
    return console.log(longestWord);
}
findLongestWord(avengers);
*/

//Ejercicio 3
//Implemente la función denominada sumNumbers que toma un array de números como argumento y devuelve la suma de todos los números de la matriz. 

/*
const numbers = [1, 2, 3, 5, 45, 37, 58];

function sumArray(array) {
  let sum = 0;
  for (let i = 0; i < array.length; i++) {
    sum += array[i];
  }
  return console.log(sum);
}
sumArray(numbers);
*/

//Ejercicio 4
// Calcular un promedio es una tarea extremadamente común. Puedes usar este array para probar tu función:
/*
const nums = [12, 21, 38, 5, 45, 37, 6];

function average(param) {
  let sum = 0;
  for (let i = 0; i < param.length; i++) {
    sum += param[i];
  }
  return console.log(sum / param.length);
}
average(nums);
*/

//Ejercicio 5
// Crea una función que reciba por parámetro un array y cuando es un valor number lo sume y de lo contrario cuente la longitud del string y lo sume. Puedes usar este array para probar tu función:
/*
const mixedElements = [6, 1, "Rayo", 1, "vallecano", "10", "upgrade", 8, "hub"];

function averageWord(param) {
  let sum = 0;
  for (let i = 0; i < param.length; i++) {
    if (typeof param[i] === "string") {
      sum += param[i].length;
    }
  }
  return console.log(sum / param.length);
}
averageWord(mixedElements);
*/

// Ejercicio 6
//Crea una función que reciba por parámetro un array y compruebe si existen elementos duplicados, en caso que existan los elimina para retornar un array sin los elementos duplicados. Puedes usar este array para probar tu función:

/*
const duplicates = [
  "sushi",
  "pizza",
  "burger",
  "potatoe",
  "pasta",
  "ice-cream",
  "pizza",
  "chicken",
  "onion rings",
  "pasta",
  "soda",
];

function deleteDuplicates(param) {
  let unique = [];
  for (let i = 0; i < param.length; i++) {
    if (!unique.includes(param[i])) {
      unique.push(param[i]);
    }
  }
  return console.log(unique);
}
deleteDuplicates(duplicates);
*/

//Ejercicio 7
//Crea una función que reciba por parámetro un array y el valor que desea comprobar que existe dentro de dicho array - comprueba si existe el elemento, en caso que existan nos devuelve un true y la posición de dicho elemento y por la contra un false. Puedes usar este array para probar tu función:
/*
const nameFinder = [
    "Peter",
    "Steve",
    "Tony",
    "Natasha",
    "Clint",
    "Logan",
    "Xabier",
    "Bruce",
    "Peggy",
    "Jessica",
    "Marc",
  ];
  
  function findName(param, value) {
    for (let i = 0; i < param.length; i++) {
      if (param[i] === value) {
        return console.log(true, i);
      }
    }
    return console.log(false);
  }
  findName(nameFinder, "Natasha");
  */

